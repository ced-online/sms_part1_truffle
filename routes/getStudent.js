var express = require('express');
var router = express.Router();

router.get('/', function (req, res, next) {
    data = req.query;
    console.log(data);
    web3.eth.getAccounts().then((accounts)=>{
    SMS.methods.getStudent(data.roll)
        .call({ from: accounts[0] }).then((val) => {
            console.log(val);
            val._age = web3.utils.toBN(val._age).toString();
            res.render("getStudent", {myData : val});
        })
    })
});

module.exports = router;
