pragma solidity ^0.5.16;

contract StudentManagementSystem {
    
    enum gender {male, female, other}
    
    struct profile {
        string name;
        uint age;
        bool indian;
        gender myGender;
    }
    
    mapping(uint => profile) student;
    
    function setStudent(uint _roll, string memory _name, uint _age, bool _indian, gender _myGender) public {
        student[_roll] = profile(_name, _age, _indian, _myGender);
    }
    
    function getStudent(uint _roll) public view returns (string memory _name, uint _age, bool _indian, gender _myGender){
        _name = student[_roll].name;
        _age = student[_roll].age;
        _indian = student[_roll].indian;
        _myGender = student[_roll].myGender;
    }
    
}
