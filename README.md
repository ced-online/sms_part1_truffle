# Student Management System



## Setting up the development environment 

Before we start we need some tools and dependencies. Please install the following:

1. Node.js and npm (comes with Node)
2. Git
3. Geth

## Running the geth client 
$ geth --dev --networkid "4002" --rpc --rpcport 8545 --rpccorsdomain "*" --rpcapi " personal,
db, eth, net, web3"

## Compiling and deploying the smart contract.
Go to: http://remix.ethereum.org
1. Compile the contract
2. Connect to the geth client using the Web3 Provider environment in remix and
deploy the contract


## Edit app.js
Provide coinbase and contract address


## Installation
Install all dependencies using following command <br />

$ npm install

## Run Dapp
Using following command, we can run the application<br />
$ npm start



